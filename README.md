# README #

Appdvendor is a RESTful web-service to handle subscription and user events
as part of the integration between the vendor and AppDirect. 

### Maven project composed of three modules:  ###

* Event handling web-service module 'appdvendor-event-handler-ws'
* Rest client module 'appdvendor-rest-client'
* App Direct client module 'appdvendor-appd-client'

### Features ###

* The event handling WS receives AppDirect events and keep the vendor's users and subscriptions in sync
* The service can respond to the following events: SUBSCRIPTION_ORDER, SUBSCRIPTION_CHANGE, SUBSCRIPTION_CANCEL, USER_ASSIGNMENT, USER_UNASSIGNMENT, and USER_UPDATED
* For the end-point /events/handle, only incoming requests sent by AppDirect and signed with the proper OAuth key and secret are accepted by default. You will get a 401 error response otherwise. if you want to test directly on the URL for some reason, you can disable this behavior by changing the following property in the build file: appd.oauth.filter.ignoreMissingCredentials=true
* All outgoing requests from the service towards AppDirect API are signed by the OAuth key and secret too

### How do I get set up on my machine? ###

* create a PostgreSQL DB 'appdvendor' as per the following SQL script /appdvendor-event-handler-ws/src/db/db-postgres.sql
* Check out the project from this repo to your local machine
* cd appdvendor-parent
* mvn clean package -Denv=dev or -Denv=prod depending on the desired build profile
* Deploy the WAR file located under /appdvendor-event-handler-ws/target/appdvendor-event-handler-ws.war to your web server(Tomcat or Jetty for instance)