package com.appdvendor.rest.api.response;

import org.springframework.http.HttpStatus;

/**
 * A response model to encapsulate the result and errors while
 * communicating with a rest API service. 
 * 
 * @author Muthanna
 */
public class ApiResponse<T> {

    private final T result;
    private final Error error;
    private final HttpStatus status;

    public ApiResponse(T result, HttpStatus status) {
        this(result, null, status);
    }
    
    public ApiResponse(Error error, HttpStatus status) {
        this(null, error, status);
    }
    
    public ApiResponse(T result, Error error, HttpStatus status) {
        this.result = result;
        this.error = error;
        this.status = status;
    }

    public T getResult() {
        return result;
    }

    public Error getError() {
		return error;
	}
    
    public HttpStatus getStatus() {
		return status;
	}

}
