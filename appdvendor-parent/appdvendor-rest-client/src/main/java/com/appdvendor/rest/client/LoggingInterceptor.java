package com.appdvendor.rest.client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpRequest;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;

/**
 * An interceptor to help logging request/response.
 * 
 * @author Muthanna
 */
public class LoggingInterceptor implements ClientHttpRequestInterceptor {

	private final static Logger log = LogManager.getLogger(LoggingInterceptor.class);

	private final static String ENCODING = "UTF-8";

	@Override
	public ClientHttpResponse intercept(HttpRequest request, byte[] body, ClientHttpRequestExecution execution)
			throws IOException {

		// Log request
		log.debug("Sending a " + request.getMethod()+ " request to: " + request.getURI());
		log.debug("And body: " + new String(body, ENCODING));

		// Execute
		ClientHttpResponse response = execution.execute(request, body);

		// Log response
		StringBuilder responseBuilder = new StringBuilder();
		BufferedReader buffer = new BufferedReader(new InputStreamReader(response.getBody(), ENCODING));
		String line = buffer.readLine();
		while (line != null) {
			responseBuilder.append(line);
			responseBuilder.append('\n');
			line = buffer.readLine();
		}

		log.debug("Received a reponse with status code: " + response.getStatusCode());
		log.debug("And status message: " + response.getStatusText());
		log.debug("And body: " + responseBuilder.toString());

		return response;
	}

}