package com.appdvendor.rest.api.response;

import java.io.Serializable;

/**
 * An error model to representing the issues encountered while sending
 * requests and receiving responses.
 * 
 * @author Muthanna
 */
public class Error implements Serializable {

	private static final long serialVersionUID = 3518244153508966819L;
	
	private String code;
	private String message;
	private ErrorType type;
	
	public Error() {}
	
	public Error(String message, ErrorType type) {
		this(null, message, ErrorType.UNKNOWN);
	}
	
	public Error(String code, String message, ErrorType type) {
		this.code = code;
		this.message = message;
		this.type = type;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	
	public ErrorType getType() {
		return type;
	}
	
	public void setType(ErrorType type) {
		this.type = type;
	}

}
