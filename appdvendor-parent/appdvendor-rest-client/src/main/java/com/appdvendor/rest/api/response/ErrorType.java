package com.appdvendor.rest.api.response;

/**
 * Enumerates the general errors that might occur when
 * connecting to a rest API service. 
 * @author Muthanna
 */
public enum ErrorType {

	// Validation related codes. Mainly when returning 400 http code
	VALIDATION_ERROR,

	// Authentication related codes when returning 401 http code
	AUTH_BAD_CREDENTIALS,
	
	// Limit reached
	LIMIT_REACHED,
	
	// Internal server error occurred. 500 http code
	BACKEND_ERROR,
	
	// General Runtime errors
	CONNECTION_ERROR, 
	SERIALIZATION_ERROR,
	
	// Unknown
	UNKNOWN
	
}
