package com.appdvendor.rest.client;

import org.springframework.http.client.BufferingClientHttpRequestFactory;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.security.oauth.common.signature.SharedConsumerSecretImpl;
import org.springframework.security.oauth.consumer.BaseProtectedResourceDetails;
import org.springframework.security.oauth.consumer.client.OAuthRestTemplate;
import org.springframework.web.client.RestTemplate;

/**
 * Relies on {@link OAuthRestTemplate} and sign the with  consumer key and secret.
 * 
 * @author Muthanna
 */
public class OAuthRestClient extends RestClient {

	private String consumerKey;
	private String consumerSecret;

	public void setConsumerKey(String consumerKey) {
		this.consumerKey = consumerKey;
	}

	public void setConsumerSecret(String consumerSecret) {
		this.consumerSecret = consumerSecret;
	}

	@Override
	protected RestTemplate getTemplateToUse(ClientHttpRequestFactory requestFactory) {

		final BaseProtectedResourceDetails resource = new BaseProtectedResourceDetails();
		resource.setConsumerKey(consumerKey);
		resource.setSharedSecret(new SharedConsumerSecretImpl(consumerSecret));

		return new OAuthRestTemplate(new BufferingClientHttpRequestFactory(requestFactory), resource);
	}

}
