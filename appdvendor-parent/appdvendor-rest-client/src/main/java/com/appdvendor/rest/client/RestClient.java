package com.appdvendor.rest.client;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.client.BufferingClientHttpRequestFactory;
import org.springframework.http.client.ClientHttpRequest;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RequestCallback;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.ResponseErrorHandler;
import org.springframework.web.client.ResponseExtractor;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.appdvendor.rest.api.response.ApiResponse;
import com.appdvendor.rest.api.response.Error;
import com.appdvendor.rest.api.response.ErrorType;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * A REST client that is based on {@link RestTemplate} and {@link HttpClient}.
 * It is designed initialize the connection and settings and to the main facade
 * on the client side to communicate with the server API.
 *  
 * @author Muthanna
 */
public class RestClient implements InitializingBean {

	private final static Logger logger = LogManager.getLogger(RestClient.class);
	
    private ObjectMapper objectMapper;

    private String apiBaseUrl;
    private int connectionTimeoutMilliseconds;
    private int readTimeoutMilliseconds;
    private int maxTotalConnections;
    
    private RestTemplate restTemplate;
    
    public ObjectMapper getObjectMapper() {
		return objectMapper;
	}
    
    public void setObjectMapper(ObjectMapper objectMapper) {
    	//objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		this.objectMapper = objectMapper;
	}

    public String getApiBaseUrl() {
		return apiBaseUrl;
	}
    
	public void setApiBaseUrl(String apiBaseUrl) {
		this.apiBaseUrl = apiBaseUrl;
	}

	public int getConnectionTimeoutMilliseconds() {
		return connectionTimeoutMilliseconds;
	}
	
	public void setConnectionTimeoutMilliseconds(int connectionTimeoutMilliseconds) {
		this.connectionTimeoutMilliseconds = connectionTimeoutMilliseconds;
	}

	public int getReadTimeoutMilliseconds() {
		return readTimeoutMilliseconds;
	}
	
	public void setReadTimeoutMilliseconds(int readTimeoutMilliseconds) {
		this.readTimeoutMilliseconds = readTimeoutMilliseconds;
	}

	public int getMaxTotalConnections() {
		return maxTotalConnections;
	}
	
	public void setMaxTotalConnections(int maxTotalConnections) {
		this.maxTotalConnections = maxTotalConnections;
	}
	
	protected RestTemplate getTemplateToUse(ClientHttpRequestFactory requestFactory) {
		return new RestTemplate(new BufferingClientHttpRequestFactory(requestFactory));
	}

	public void afterPropertiesSet() throws Exception {
        try {
        	
			// Config HTTP client
			PoolingHttpClientConnectionManager connectionManager = new PoolingHttpClientConnectionManager();
			connectionManager.setMaxTotal(maxTotalConnections);
			connectionManager.setDefaultMaxPerRoute(maxTotalConnections);
			RequestConfig requestConfig = RequestConfig.custom().setConnectTimeout(connectionTimeoutMilliseconds)
					.setSocketTimeout(readTimeoutMilliseconds).build();

			HttpClient httpClient = HttpClients.custom().setDefaultRequestConfig(requestConfig)
					.setConnectionManager(connectionManager).build();

			// Config Rest template
			// Request factory
			HttpComponentsClientHttpRequestFactory requestFactory = new HttpComponentsClientHttpRequestFactory(httpClient);
			
			RestTemplate template = getTemplateToUse(requestFactory);
			
			List<ClientHttpRequestInterceptor> interceptors = new ArrayList<ClientHttpRequestInterceptor>();
			interceptors.add(new LoggingInterceptor());
			template.setInterceptors(interceptors);
			
			// Converters
			FormHttpMessageConverter formHttpMessageConverter = new FormHttpMessageConverter();
            formHttpMessageConverter.setCharset(Charset.forName("UTF8"));
			template.getMessageConverters().add(formHttpMessageConverter);
			
			// Error handler
			template.setErrorHandler(new ResponseErrorHandler() {
				@Override
				public boolean hasError(ClientHttpResponse resp) throws IOException {
					return false; // This is to allow handling errors in a custom way 
				}

				@Override
				public void handleError(ClientHttpResponse resp) throws IOException {
				}
			});
			
			this.restTemplate = template;
		} catch (Exception e) {
			logger.error("An error occured while attempting to initialize Rest Client:" + e);
			throw e;
		}
    }
    
    protected <T> ApiResponse<T> sendAndProcess(final String url, final HttpMethod method, final Object request, 
    		final Class<T> resultClass) {
    	return sendAndProcess(url, method, request, resultClass, Collections.emptyMap());
    }
    
    protected <T> ApiResponse<T> sendAndProcess(final String url, final HttpMethod method, final Object request, 
    		final Class<T> resultClass, final Map<String, List<String>> headers) {
    	
        try {
        	// Build the request callback
        	final RequestCallback requestCallback = new RequestCallback() {
                @Override
                public void doWithRequest(ClientHttpRequest clientHttpRequest) throws IOException {
                    // Additional dynamic headers
                    clientHttpRequest.getHeaders().putAll(headers);
                    clientHttpRequest.getHeaders().setContentType(MediaType.APPLICATION_JSON);
                    clientHttpRequest.getHeaders().setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
                	if (request != null) {
                        try {
                            objectMapper.writeValue(clientHttpRequest.getBody(), request);
                        } catch (Exception e) {
                        	logger.error("An error occured while attempting to send request: {}", e);
                            throw new IOException(e);
                        }
                    }
                }
            };
        	
        	// Define the response extractor
            final ResponseExtractor<ApiResponse<T>> extractor = new ResponseExtractor<ApiResponse<T>>() {
    			@Override
                public ApiResponse<T> extractData(ClientHttpResponse response) throws IOException {
                	HttpStatus status = response.getStatusCode();
                	boolean httpSuccessStatus = status.value() >= HttpStatus.OK.value() && status.value() < HttpStatus.BAD_REQUEST.value();
                	boolean httpUnauthorized = status.value() == HttpStatus.UNAUTHORIZED.value() || status.value() == HttpStatus.FORBIDDEN.value();

                	if(httpUnauthorized) {
                    	Error error = new Error("Unauthorized", ErrorType.AUTH_BAD_CREDENTIALS);
                        return new ApiResponse<T>(error, status);
                	}
                	
                    try {
                    	ApiResponse<T> apiResponse = null;
                		if(httpSuccessStatus) {
                			if(resultClass != Void.class) {
                            	apiResponse =  new ApiResponse<T>(objectMapper.readValue(response.getBody(), resultClass), status);
                			} else {
                				apiResponse = new ApiResponse<>(null, status);
                			}
                        	
                        } else {
                        	Error error = objectMapper.readValue(response.getBody(), Error.class);
                        	error.setType(ErrorType.VALIDATION_ERROR);
                        	apiResponse =  new ApiResponse<T>(error, status);
                        }
                    	
                        return apiResponse;
                    } catch (Exception ex) {
                    	// We got JSON mapping or parsing errors
                    	logger.error(ex);
                    	Error error = new Error("Serialization error occured", ErrorType.SERIALIZATION_ERROR);
                        return new ApiResponse<T>(error, status);
                    }
                }
            };
            
            // Send to the target URL with the defined method, request callback and response extractor
            ApiResponse<T> resp = restTemplate.execute(url, method, requestCallback, extractor);
            
            return resp;
        } catch (RestClientException e) {
        	logger.error(e);
        	ErrorType type = ErrorType.UNKNOWN;
        	if(e instanceof ResourceAccessException)
        		type = ErrorType.CONNECTION_ERROR;
        	else if (e instanceof HttpClientErrorException)
        		type = ErrorType.VALIDATION_ERROR;
        	else if (e instanceof HttpServerErrorException)
        		type = ErrorType.BACKEND_ERROR;
        	
        	Error error = new Error("Error occured while connecting to server", type);
            return new ApiResponse<T>(error, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    
}
