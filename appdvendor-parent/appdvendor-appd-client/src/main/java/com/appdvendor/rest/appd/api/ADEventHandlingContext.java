package com.appdvendor.rest.appd.api;

/**
 * AppDirect event handling context.
 * 
 * @author Muthanna
 */
public class ADEventHandlingContext {

	private String eventUrl;
	private String token;

	public String getEventUrl() {
		return eventUrl;
	}

	public void setEventUrl(String eventUrl) {
		this.eventUrl = eventUrl;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((eventUrl == null) ? 0 : eventUrl.hashCode());
		result = prime * result + ((token == null) ? 0 : token.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ADEventHandlingContext other = (ADEventHandlingContext) obj;
		if (eventUrl == null) {
			if (other.eventUrl != null)
				return false;
		} else if (!eventUrl.equals(other.eventUrl))
			return false;
		if (token == null) {
			if (other.token != null)
				return false;
		} else if (!token.equals(other.token))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "ADEventHandlingContext [eventUrl=" + eventUrl + ", token=" + token + "]";
	}
	
}
