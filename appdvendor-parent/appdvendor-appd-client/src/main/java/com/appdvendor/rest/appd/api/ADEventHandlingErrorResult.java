package com.appdvendor.rest.appd.api;

/**
 * AppDirect event handling error result.
 * 
 * @author Muthanna
 */
public class ADEventHandlingErrorResult extends ADEventHandlingResult {

	private ADEventHandlingError errorCode;
	private String message;
	
	public ADEventHandlingErrorResult(ADEventHandlingError errorCode) {
		this(errorCode, null);
	}
	
	public ADEventHandlingErrorResult(ADEventHandlingError errorCode, String message) {
		super(false);
		this.errorCode = errorCode;
		this.message = message;
	}
	
	public ADEventHandlingError getErrorCode() {
		return errorCode;
	}
	
	public void setErrorCode(ADEventHandlingError errorCode) {
		this.errorCode = errorCode;
	}
	
	public String getMessage() {
		return message;
	}
	
	public void setMessage(String message) {
		this.message = message;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((errorCode == null) ? 0 : errorCode.hashCode());
		result = prime * result + ((message == null) ? 0 : message.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		ADEventHandlingErrorResult other = (ADEventHandlingErrorResult) obj;
		if (errorCode != other.errorCode)
			return false;
		if (message == null) {
			if (other.message != null)
				return false;
		} else if (!message.equals(other.message))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "ADEventHandlingErrorResult [errorCode=" + errorCode + ", message=" + message + ", isSuccess()="
				+ isSuccess() + "]";
	}
	
}
