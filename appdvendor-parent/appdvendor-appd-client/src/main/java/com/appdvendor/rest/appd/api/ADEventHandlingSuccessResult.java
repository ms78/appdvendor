package com.appdvendor.rest.appd.api;

/**
 * AppDirect event handling success result.
 * 
 * @author Muthanna
 */
public class ADEventHandlingSuccessResult extends ADEventHandlingResult {

	private String accountIdentifier;

	public ADEventHandlingSuccessResult() {
		this(null);
	}
	
	public ADEventHandlingSuccessResult(String accountIdentifier) {
		super(true);
		this.accountIdentifier = accountIdentifier;
	}
	
	public String getAccountIdentifier() {
		return accountIdentifier;
	}

	public void setAccountIdentifier(String accountIdentifier) {
		this.accountIdentifier = accountIdentifier;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((accountIdentifier == null) ? 0 : accountIdentifier.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		ADEventHandlingSuccessResult other = (ADEventHandlingSuccessResult) obj;
		if (accountIdentifier == null) {
			if (other.accountIdentifier != null)
				return false;
		} else if (!accountIdentifier.equals(other.accountIdentifier))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "ADEventHandlingSuccessResult [accountIdentifier=" + accountIdentifier + ", isSuccess()=" + isSuccess() + "]";
	}
	
}
