package com.appdvendor.rest.appd.api;

/**
 * AppDirect event flag enum.
 * 
 * @author Muthanna
 */
public enum ADEventFlag {
	
	STATELESS, DEVELOPMENT;
	
}
