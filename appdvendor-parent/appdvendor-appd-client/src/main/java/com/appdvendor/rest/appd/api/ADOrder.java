package com.appdvendor.rest.appd.api;

import java.util.ArrayList;
import java.util.List;

/**
 * AppDirect order model.
 * 
 * @author Muthanna
 */
public class ADOrder {

	private String editionCode;
	private String addonOfferingCode;
	private String pricingDuration;
	private List<ADItem> items;

	public String getEditionCode() {
		return editionCode;
	}

	public void setEditionCode(String editionCode) {
		this.editionCode = editionCode;
	}

	public String getAddonOfferingCode() {
		return addonOfferingCode;
	}

	public void setAddonOfferingCode(String addonOfferingCode) {
		this.addonOfferingCode = addonOfferingCode;
	}

	public String getPricingDuration() {
		return pricingDuration;
	}

	public void setPricingDuration(String pricingDuration) {
		this.pricingDuration = pricingDuration;
	}

	public List<ADItem> getItems() {
		return items == null ? new ArrayList<>() : items;
	}

	public void setItems(List<ADItem> items) {
		this.items = items;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((addonOfferingCode == null) ? 0 : addonOfferingCode.hashCode());
		result = prime * result + ((editionCode == null) ? 0 : editionCode.hashCode());
		result = prime * result + ((items == null) ? 0 : items.hashCode());
		result = prime * result + ((pricingDuration == null) ? 0 : pricingDuration.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ADOrder other = (ADOrder) obj;
		if (addonOfferingCode == null) {
			if (other.addonOfferingCode != null)
				return false;
		} else if (!addonOfferingCode.equals(other.addonOfferingCode))
			return false;
		if (editionCode == null) {
			if (other.editionCode != null)
				return false;
		} else if (!editionCode.equals(other.editionCode))
			return false;
		if (items == null) {
			if (other.items != null)
				return false;
		} else if (!items.equals(other.items))
			return false;
		if (pricingDuration == null) {
			if (other.pricingDuration != null)
				return false;
		} else if (!pricingDuration.equals(other.pricingDuration))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "ADOrder [editionCode=" + editionCode + ", addonOfferingCode=" + addonOfferingCode + ", pricingDuration="
				+ pricingDuration + ", items=" + items + "]";
	}

}
