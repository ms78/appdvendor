package com.appdvendor.rest.appd.client;

import org.springframework.http.HttpMethod;

import com.appdvendor.rest.api.response.ApiResponse;
import com.appdvendor.rest.appd.api.ADCompany;
import com.appdvendor.rest.appd.api.ADEvent;
import com.appdvendor.rest.appd.api.ADEventHandlingContext;
import com.appdvendor.rest.client.OAuthRestClient;

/**
 * For AppDirect, we are extending {@link OAuthRestClient} to add support to
 * AppDirect API specific operations. Further customization can be made if needed
 * to adjust the parent client behavior.
 * 
 * @author Muthanna
 */
public class ADRestClient extends OAuthRestClient {

	// Event handling
	public ApiResponse<ADEvent> getEvent(ADEventHandlingContext context) {
		return sendAndProcess(context.getEventUrl(), HttpMethod.GET, null, ADEvent.class);
	}

	// Company ops
	public ApiResponse<ADCompany> createCompany(ADCompany company) {
		return sendAndProcess(getApiBaseUrl() + "/account/v1/companies", HttpMethod.POST, company, ADCompany.class);
	}

	public ApiResponse<ADCompany> getCompany(String uuid) {
		return sendAndProcess(getApiBaseUrl() + "/account/v1/companies/" + uuid, HttpMethod.GET, null, ADCompany.class);
	}

	public ApiResponse<?> deleteCompany(String uuid) {
		return sendAndProcess(getApiBaseUrl() + "/account/v1/companies/" + uuid, HttpMethod.DELETE, null, Void.class);
	}

	public ApiResponse<ADCompany> updateCompany(ADCompany company) {
		return sendAndProcess(getApiBaseUrl() + "/account/v1/companies/" + company.getUuid(), HttpMethod.PUT, company,
				ADCompany.class);
	}

}
