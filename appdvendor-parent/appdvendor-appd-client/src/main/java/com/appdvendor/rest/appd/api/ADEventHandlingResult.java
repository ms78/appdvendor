package com.appdvendor.rest.appd.api;

/**
 * AppDirect event handling result abstract class.
 * 
 * @author Muthanna
 */
public abstract class ADEventHandlingResult {

	private boolean success;
	
	protected ADEventHandlingResult(boolean success) {
		this.success = success;
	}
	
	public boolean isSuccess() {
		return success;
	}
	
	public void setSuccess(boolean success) {
		this.success = success;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (success ? 1231 : 1237);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ADEventHandlingResult other = (ADEventHandlingResult) obj;
		if (success != other.success)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "ADEventHandlingResult [success=" + success + "]";
	}
	
}
