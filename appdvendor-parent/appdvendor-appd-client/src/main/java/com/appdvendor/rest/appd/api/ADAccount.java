package com.appdvendor.rest.appd.api;

/**
 * AppDirect account model.
 *  
 * @author Muthanna
 */
public class ADAccount {

	private String accountIdentifier;
	private ADAccountStatus status;

	public String getAccountIdentifier() {
		return accountIdentifier;
	}

	public void setAccountIdentifier(String value) {
		this.accountIdentifier = value;
	}

	public ADAccountStatus getStatus() {
		return status;
	}

	public void setStatus(ADAccountStatus status) {
		this.status = status;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((accountIdentifier == null) ? 0 : accountIdentifier.hashCode());
		result = prime * result + ((status == null) ? 0 : status.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ADAccount other = (ADAccount) obj;
		if (accountIdentifier == null) {
			if (other.accountIdentifier != null)
				return false;
		} else if (!accountIdentifier.equals(other.accountIdentifier))
			return false;
		if (status != other.status)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "ADAccount [accountIdentifier=" + accountIdentifier + ", status=" + status + "]";
	}

}
