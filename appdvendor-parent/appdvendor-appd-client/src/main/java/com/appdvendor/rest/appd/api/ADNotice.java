package com.appdvendor.rest.appd.api;

/**
 * AppDirect subscription notice model.
 * 
 * @author Muthanna
 */
public class ADNotice {

	private ADNoticeType type;
	private String message;

	public ADNoticeType getType() {
		return type;
	}

	public void setType(ADNoticeType type) {
		this.type = type;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((message == null) ? 0 : message.hashCode());
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ADNotice other = (ADNotice) obj;
		if (message == null) {
			if (other.message != null)
				return false;
		} else if (!message.equals(other.message))
			return false;
		if (type != other.type)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "ADNotice [type=" + type + ", message=" + message + "]";
	}

}
