package com.appdvendor.rest.appd.api;

/**
 * AppDirect event type enum.
 * 
 * @author Muthanna
 */
public enum ADEventType {

	SUBSCRIPTION_ORDER,
	SUBSCRIPTION_CHANGE,
	SUBSCRIPTION_CANCEL,
	SUBSCRIPTION_NOTICE,
	USER_ASSIGNMENT,
	USER_UNASSIGNMENT,
	USER_UPDATED;
}
