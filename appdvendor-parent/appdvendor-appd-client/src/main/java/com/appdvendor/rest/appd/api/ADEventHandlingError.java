package com.appdvendor.rest.appd.api;

/**
 * AppDirect event handling error enum.
 * 
 * @author Muthanna
 */
public enum ADEventHandlingError {
   
	USER_ALREADY_EXISTS,
    USER_NOT_FOUND,
    ACCOUNT_NOT_FOUND,
    MAX_USERS_REACHED,
    UNAUTHORIZED,
    OPERATION_CANCELED,
    CONFIGURATION_ERROR,
    INVALID_RESPONSE,
    PENDING,
    FORBIDDEN,
    BINDING_NOT_FOUND,
    TRANSPORT_ERROR,
    UNKNOWN_ERROR;
	
}
