package com.appdvendor.rest.appd.api;

public enum ADAccountStatus {

	INITIALIZED, FAILED, FREE_TRIAL, FREE_TRIAL_EXPIRED, ACTIVE, SUSPENDED, CANCELLED;

}
