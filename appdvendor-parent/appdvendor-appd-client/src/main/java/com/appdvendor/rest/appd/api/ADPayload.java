package com.appdvendor.rest.appd.api;

/**
 * AppDirect event payload model.
 * 
 * @author Muthanna
 */
public class ADPayload {

	private ADAccount account;
	private ADCompany company;
	private ADOrder order;
	private ADUser user;
	private ADNotice notice;

	public ADAccount getAccount() {
		return account;
	}

	public void setAccount(ADAccount account) {
		this.account = account;
	}

	public ADCompany getCompany() {
		return company;
	}

	public void setCompany(ADCompany company) {
		this.company = company;
	}

	public ADOrder getOrder() {
		return order;
	}

	public void setOrder(ADOrder order) {
		this.order = order;
	}

	public ADUser getUser() {
		return user;
	}

	public void setUser(ADUser user) {
		this.user = user;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((account == null) ? 0 : account.hashCode());
		result = prime * result + ((company == null) ? 0 : company.hashCode());
		result = prime * result + ((notice == null) ? 0 : notice.hashCode());
		result = prime * result + ((order == null) ? 0 : order.hashCode());
		result = prime * result + ((user == null) ? 0 : user.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ADPayload other = (ADPayload) obj;
		if (account == null) {
			if (other.account != null)
				return false;
		} else if (!account.equals(other.account))
			return false;
		if (company == null) {
			if (other.company != null)
				return false;
		} else if (!company.equals(other.company))
			return false;
		if (notice == null) {
			if (other.notice != null)
				return false;
		} else if (!notice.equals(other.notice))
			return false;
		if (order == null) {
			if (other.order != null)
				return false;
		} else if (!order.equals(other.order))
			return false;
		if (user == null) {
			if (other.user != null)
				return false;
		} else if (!user.equals(other.user))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "ADPayload [account=" + account + ", company=" + company + ", order=" + order + ", user=" + user
				+ ", notice=" + notice + "]";
	}

}
