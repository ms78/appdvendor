package com.appdvendor.rest.appd.api;

/**
 * AppDirect subscription notice type enum.
 * 
 * @author Muthanna
 */
public enum ADNoticeType {
	 REACTIVATED, DEACTIVATED, CLOSED, UPCOMING_INVOICE;
}
