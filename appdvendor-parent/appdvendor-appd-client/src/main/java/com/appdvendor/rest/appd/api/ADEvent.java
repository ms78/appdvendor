package com.appdvendor.rest.appd.api;

/**
 * AppDirect event model.
 * 
 * @author Muthanna
 */
public class ADEvent {

	private ADEventType type;
	private ADMarketplace marketplace;
	private String applicationUuid;
	private ADEventFlag flag;
	private ADUser creator;
	private ADPayload payload;
	private String returnUrl;

	public ADEventType getType() {
		return type;
	}

	public void setType(ADEventType type) {
		this.type = type;
	}

	public ADMarketplace getMarketplace() {
		return marketplace;
	}

	public void setMarketplace(ADMarketplace marketplace) {
		this.marketplace = marketplace;
	}

	public String getApplicationUuid() {
		return applicationUuid;
	}

	public void setApplicationUuid(String applicationUuid) {
		this.applicationUuid = applicationUuid;
	}

	public ADEventFlag getFlag() {
		return flag;
	}

	public void setFlag(ADEventFlag flag) {
		this.flag = flag;
	}

	public ADUser getCreator() {
		return creator;
	}

	public void setCreator(ADUser creator) {
		this.creator = creator;
	}

	public ADPayload getPayload() {
		return payload;
	}

	public void setPayload(ADPayload payload) {
		this.payload = payload;
	}

	public String getReturnUrl() {
		return returnUrl;
	}

	public void setReturnUrl(String returnUrl) {
		this.returnUrl = returnUrl;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((applicationUuid == null) ? 0 : applicationUuid.hashCode());
		result = prime * result + ((creator == null) ? 0 : creator.hashCode());
		result = prime * result + ((flag == null) ? 0 : flag.hashCode());
		result = prime * result + ((marketplace == null) ? 0 : marketplace.hashCode());
		result = prime * result + ((payload == null) ? 0 : payload.hashCode());
		result = prime * result + ((returnUrl == null) ? 0 : returnUrl.hashCode());
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ADEvent other = (ADEvent) obj;
		if (applicationUuid == null) {
			if (other.applicationUuid != null)
				return false;
		} else if (!applicationUuid.equals(other.applicationUuid))
			return false;
		if (creator == null) {
			if (other.creator != null)
				return false;
		} else if (!creator.equals(other.creator))
			return false;
		if (flag != other.flag)
			return false;
		if (marketplace == null) {
			if (other.marketplace != null)
				return false;
		} else if (!marketplace.equals(other.marketplace))
			return false;
		if (payload == null) {
			if (other.payload != null)
				return false;
		} else if (!payload.equals(other.payload))
			return false;
		if (returnUrl == null) {
			if (other.returnUrl != null)
				return false;
		} else if (!returnUrl.equals(other.returnUrl))
			return false;
		if (type != other.type)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "ADEvent [type=" + type + ", marketplace=" + marketplace + ", applicationUuid=" + applicationUuid
				+ ", flag=" + flag + ", creator=" + creator + ", payload=" + payload + ", returnUrl=" + returnUrl + "]";
	}

}
