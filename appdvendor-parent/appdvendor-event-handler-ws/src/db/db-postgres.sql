-- SQL to create appdvendor database
 
CREATE DATABASE appdvendor WITH ENCODING 'UTF8';

CREATE TABLE IF NOT EXISTS market_place (
  id serial NOT NULL PRIMARY KEY,
  base_url varchar(254),
  partner varchar(100)
);

insert into market_place(base_url, partner) values('https://marketplace.appdirect.com', 'APPDIRECT');

CREATE TABLE IF NOT EXISTS company (
  id serial NOT NULL PRIMARY KEY,
  uuid varchar(254),
  name varchar(100) NOT NULL,
  email varchar(254),
  phone varchar(20),
  website varchar(254),
  country varchar(100)
);

CREATE TABLE IF NOT EXISTS user_account (
  id serial NOT NULL PRIMARY KEY,
  uuid varchar(254),
  type char(1) NOT NULL DEFAULT 'U', -- U for user, A for ADMIN
  email varchar(254) NOT NULL,
  created timestamptz NOT NULL DEFAULT now(),
  first_name varchar(100),
  last_name varchar(100),
  open_id varchar(254),
  language char(2) NOT NULL DEFAULT 'EN',
  company_id int,
  CONSTRAINT unq_user_account_email UNIQUE (email),
  CONSTRAINT fk_user_account_company_id FOREIGN KEY (company_id) REFERENCES company (id)
);

CREATE TABLE IF NOT EXISTS product (
  id serial NOT NULL PRIMARY KEY,
  uuid varchar(254),
  name varchar(100) NOT NULL,
  service_type varchar(50) DEFAULT 'STANDALONE_PRODUCT',
  usage_model varchar(50) DEFAULT 'SIGNLE_USER',
  revenue_model varchar(50) DEFAULT 'FREE',
  market_place_id int,
  CONSTRAINT fk_product_market_place_id FOREIGN KEY (market_place_id) REFERENCES market_place (id)
);
insert into product(uuid, name, market_place_id) values('44ub8f15-1af6-5bfc-9a6d-8061d45a0c55', 'My App', 1);

CREATE TABLE IF NOT EXISTS plan (
  id serial NOT NULL PRIMARY KEY,
  name varchar(100) NOT NULL,
  uuid varchar(254),
  edition_code varchar(100) NOT NULL,
  pricing_duration varchar(100),
  revenue_model varchar(50) DEFAULT 'FREE',
  product_id int,
  CONSTRAINT fk_plan_product_id FOREIGN KEY (product_id) REFERENCES product (id)
);

CREATE TABLE IF NOT EXISTS subscription (
  id serial NOT NULL PRIMARY KEY,
  status varchar(20) NOT NULL DEFAULT 'ACTIVE',
  market_place_id int,
  user_id int,
  company_id int,
  plan_id int,
  CONSTRAINT fk_subscription_market_place_id FOREIGN KEY (market_place_id) REFERENCES market_place (id),
  CONSTRAINT fk_subscription_user_id FOREIGN KEY (user_id) REFERENCES user_account (id),
  CONSTRAINT fk_subscription_company_id FOREIGN KEY (company_id) REFERENCES company (id),
  CONSTRAINT fk_subscription_plan_id FOREIGN KEY (plan_id) REFERENCES plan (id)
);

CREATE TABLE IF NOT EXISTS subscription_item (
  id serial NOT NULL PRIMARY KEY,
  quantity int,
  unit varchar(100),
  subscription_id int,
  CONSTRAINT fk_subscription_item_subscription_id FOREIGN KEY (subscription_id) REFERENCES subscription (id)
);

CREATE TABLE IF NOT EXISTS subscriber (
  id serial NOT NULL PRIMARY KEY,
  user_id int,
  subscription_id int,
  CONSTRAINT fk_subscriber_user_id FOREIGN KEY (user_id) REFERENCES user_account (id),
  CONSTRAINT fk_subscriber_subscription_id FOREIGN KEY (subscription_id) REFERENCES subscription (id)
);

CREATE USER appdvendor with password 'appdvendor'; -- Change it
GRANT ALL PRIVILEGES ON DATABASE appdvendor to appdvendor;
GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA public TO appdvendor;
GRANT ALL PRIVILEGES ON ALL SEQUENCES IN SCHEMA public TO appdvendor;

