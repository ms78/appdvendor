<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css"
	rel="stylesheet">
<link
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css"
	rel="stylesheet">
<link
	href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css"
	rel="stylesheet">
<style type="text/css">
.navbar-static-top {
	margin-bottom: 20px;
}

footer {
	padding: 10px;
}
</style>
</head>

<body>

	<!-- Header -->
	<div id="top-nav" class="navbar navbar-inverse navbar-static-top">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse"
				data-target=".navbar-collapse">
				<span class="icon-toggle"></span>
			</button>
			<a class="navbar-brand" href="#">AppdVendor Inc.</a>
		</div>
	</div>
	<!-- /Header -->

	<!-- Main -->
	<div class="container">
		<div class="row">

			<div class="col-md-12">
				<a href="#"><strong><span class="fa fa-dashboard"></span>
						AppdVendor Dashboard</strong></a>
				<hr>

				<div class="row">
					<div class="col-md-12">

						<div class="panel panel-default">
							<div class="panel-heading">Products</div>
							<table class="table table-striped">
								<thead>
									<tr>
										<th>uuid</th>
										<th>name</th>
										<th>service</th>
										<th>usage</th>
										<th>revenue</th>
										<th>Marketplace</th>
										<th>Plans</th>
									</tr>
								</thead>
								<tbody>
									<c:forEach items="${products}" var="product" varStatus="status">
										<tr>
											<td>${product.uuid}</td>
											<td>${product.name}</td>
											<td>${product.serviceType}</td>
											<td>${product.usageModel}</td>
											<td>${product.revenueModel}</td>
											<td>${product.marketPlace.baseUrl}</td>
											<td>
												<c:forEach items="${product.plans}" var="plan">
	                    							${plan.editionCode}-${plan.pricingDuration} <br />
												</c:forEach>
											</td>
										</tr>
									</c:forEach>
								</tbody>
							</table>
						</div>
						<!--/panel-->

						<div class="panel panel-default">
							<div class="panel-heading">Companies</div>
							<table class="table table-striped">
								<thead>
									<tr>
										<th>uuid</th>
										<th>name</th>
										<th>email</th>
										<th>phone</th>
										<th>users</th>
									</tr>
								</thead>
								<tbody>
									<c:forEach items="${companies}" var="company" varStatus="status">
										<tr>
											<td>${company.uuid}</td>
											<td>${company.name}</td>
											<td>${company.email}</td>
											<td>${company.phone}</td>
											<td>
												<c:forEach items="${company.users}" var="user">
	                    							${user.firstName} ${user.lastName} <br />
												</c:forEach>
											</td>
										</tr>
									</c:forEach>
								</tbody>
							</table>
						</div>
						<!--/panel-->

						<div class="panel panel-default">
							<div class="panel-heading">Subscriptions</div>
							<table class="table table-striped">
								<thead>
									<tr>
										<th>account</th>
										<th>status</th>
										<th>creator</th>
										<th>company</th>
										<th>plan</th>
										<th>items</th>
										<th>subscribers</th>
									</tr>
								</thead>
								<tbody>
									<c:forEach items="${subscriptions}" var="subscription" varStatus="status">
										<tr>
											<td>${subscription.id}</td>
											<td>${subscription.status}</td>
											<td>${subscription.creator.firstName} ${subscription.creator.lastName}</td>
											<td>${subscription.company.name}</td>
											<td>${subscription.plan.editionCode}</td>
											<td>
												<c:forEach items="${subscription.items}" var="item">
	                    							${item.quantity}-${item.unit} <br />
												</c:forEach>
											</td>
											<td>
												<c:forEach items="${subscription.subscribers}" var="subscriber">
	                    							${subscriber.subscriber.firstName} ${subscriber.subscriber.lastName}(${subscriber.subscriber.email}) <br />
												</c:forEach>
											</td>
										</tr>
									</c:forEach>
								</tbody>
							</table>
						</div>
						<!--/panel-->
						
					</div>

				</div>

			</div>
		</div>
	</div>
	<!-- /Main -->
		 
</body>
</html>

