package com.appdvendor.eventhandler.service.event.handler;

import com.appdvendor.eventhandler.exception.EventHandlingException;
import com.appdvendor.eventhandler.model.Subscriber;
import com.appdvendor.eventhandler.model.Subscription;
import com.appdvendor.eventhandler.model.User;
import com.appdvendor.eventhandler.util.ModelConverterUtil;
import com.appdvendor.rest.appd.api.ADEvent;
import com.appdvendor.rest.appd.api.ADEventHandlingError;
import com.appdvendor.rest.appd.api.ADEventHandlingResult;
import com.appdvendor.rest.appd.api.ADEventHandlingSuccessResult;
import com.appdvendor.rest.appd.api.ADPayload;

/**
 * Handles user assign.
 * 
 * @author Muthanna
 */
public class UserAssignEventHandler extends AbstractEventHandler {

	@Override
	public ADEventHandlingResult process(ADEvent event) throws EventHandlingException {

		// Make sure we've got a valid account
		Subscription subscription = getAccountService().getSubscriptionById(event.getPayload().getAccount().getAccountIdentifier());
		if (subscription == null) {
			throw new EventHandlingException("Account not found!", ADEventHandlingError.ACCOUNT_NOT_FOUND);
		}
		
		// Retrieve user and create her if not already done
		ADPayload payload = event.getPayload();
		User user = getAccountService().getUserByAppDirectUuid(payload.getUser().getUuid());
		if (user == null) {
			user = ModelConverterUtil.buildUser(payload.getUser(), subscription.getCompany());
			getAccountService().saveUser(user);
		}

		// Is already assigned?
		for(User u : subscription.getSubscribersUsers()) {
			if(user.equals(u)) 
				throw new EventHandlingException("User already exists!", ADEventHandlingError.USER_ALREADY_EXISTS);
		}
		
		Subscriber subscriber = new Subscriber();
		subscriber.setSubscriber(user);
		subscriber.setSubscription(subscription);
		subscription.getSubscribers().add(subscriber);
		
		getAccountService().saveSubscription(subscription);
		
		return new ADEventHandlingSuccessResult();
	}

}
