package com.appdvendor.eventhandler.web.security;

import java.util.Collections;
import java.util.List;

import javax.servlet.FilterChain;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.oauth.provider.filter.ProtectedResourceProcessingFilter;
import org.springframework.security.web.util.matcher.RequestMatcher;

/**
 * An OAuth filter to decide which request to intercept and
 * check for valid credentials.
 *  
 * @author Muthanna
 */
public class OAuthProtectedResourceProcessingFilter extends ProtectedResourceProcessingFilter {

	private List<RequestMatcher> matchers = Collections.emptyList();

	public void setMatchers(List<RequestMatcher> matchers) {
		if (matchers != null)
			this.matchers = matchers;
	}

	@Override
	protected boolean requiresAuthentication(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) {
		
		for (RequestMatcher requestMatcher : matchers) {
			if (requestMatcher.matches(request)) {
				return true;
			}
		}

		return false;
	}
}
