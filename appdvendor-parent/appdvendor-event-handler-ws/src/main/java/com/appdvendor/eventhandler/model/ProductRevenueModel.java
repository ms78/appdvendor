package com.appdvendor.eventhandler.model;

/**
 * 
 * @author Muthanna
 */
public enum ProductRevenueModel {
	FREE, 
	ONE_TIME,
	RECURRING,
	TIERED;
}
