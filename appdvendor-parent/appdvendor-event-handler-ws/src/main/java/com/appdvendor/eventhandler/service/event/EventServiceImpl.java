package com.appdvendor.eventhandler.service.event;

import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.appdvendor.eventhandler.exception.EventHandlingException;
import com.appdvendor.eventhandler.service.event.handler.EventHandler;
import com.appdvendor.rest.api.response.ApiResponse;
import com.appdvendor.rest.appd.api.ADEvent;
import com.appdvendor.rest.appd.api.ADEventHandlingContext;
import com.appdvendor.rest.appd.api.ADEventHandlingError;
import com.appdvendor.rest.appd.api.ADEventHandlingResult;
import com.appdvendor.rest.appd.api.ADEventType;
import com.appdvendor.rest.appd.client.ADRestClient;

/**
 * Implementation of {@link EventService}.
 * 
 * @author Muthanna
 */
public class EventServiceImpl implements EventService {

	protected static final Logger logger = LogManager.getLogger(EventServiceImpl.class);
	
	private ADRestClient restClient;

	private Map<ADEventType, EventHandler> eventHandlers;

	public void setRestClient(ADRestClient restClient) {
		this.restClient = restClient;
	}

	public void setEventHandlers(Map<ADEventType, EventHandler> eventHandlers) {
		this.eventHandlers = eventHandlers;
	}

	@Override
	public ADEventHandlingResult execute(ADEventHandlingContext context) throws EventHandlingException {
		
		logger.info("Received an event notification with context: {}", context );
		
		if(context.getEventUrl() == null)
			throw new EventHandlingException("Invalid event url has been passed", ADEventHandlingError.UNKNOWN_ERROR);
		
		// Fetch event from remote AppDirect API
		ApiResponse<ADEvent> apiResponse = restClient.getEvent(context);
		if(apiResponse.getError() != null) {
			throw new EventHandlingException(apiResponse.getError().getMessage(), ADEventHandlingError.TRANSPORT_ERROR);
		}
		
		// We got an event, process it
		ADEvent event = apiResponse.getResult();
		
		EventHandler handler = eventHandlers.get(event.getType());
		
		if(handler == null) {
			String err = "No proper handler is available to handle this event: " + event.getType();
			logger.error(err);
			throw new EventHandlingException(err, ADEventHandlingError.INVALID_RESPONSE);
		}
		
		ADEventHandlingResult result = handler.handle(event);
		
		logger.info("Completed handling event: {}", event.getType());
		
		return result;
		
	}

}
