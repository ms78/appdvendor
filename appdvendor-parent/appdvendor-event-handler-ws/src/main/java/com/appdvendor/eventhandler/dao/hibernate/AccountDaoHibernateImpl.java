package com.appdvendor.eventhandler.dao.hibernate;


import org.hibernate.Query;

import com.appdvendor.eventhandler.dao.AccountDao;
import com.appdvendor.eventhandler.model.Company;
import com.appdvendor.eventhandler.model.MarketPlace;
import com.appdvendor.eventhandler.model.Plan;
import com.appdvendor.eventhandler.model.Subscription;
import com.appdvendor.eventhandler.model.User;

/**
 * A Hibernate based implementation of {@link AccountDao}.
 * 
 * @author Muthanna
 */
public class AccountDaoHibernateImpl extends BaseDaoHibernateImpl implements AccountDao {
	
	@Override
	public User getUserByAppDirectUuid(String uuid) {
		 Query query = getSession().createQuery("SELECT u FROM User u WHERE u.uuid=:uuid").setParameter("uuid", uuid);
	     return  (User) query.uniqueResult();
	}
	
	@Override
	public Company getCompanyByAppDirectUuid(String uuid) {
		 Query query = getSession().createQuery("SELECT c FROM Company c WHERE c.uuid=:uuid").setParameter("uuid", uuid);
	     return  (Company) query.uniqueResult();
	}
	
	@Override
	public User getUserByOpenId(String openId) {
		Query query = getSession().createQuery("SELECT u FROM User u WHERE u.openId=:openId").setParameter("openId", openId);
        return  (User) query.uniqueResult();
	}

	@Override
	public Subscription getSubscriptionById(String id) {
		return getSession().get(Subscription.class, Long.parseLong(id));
	}

	@Override
	public Plan getPlanByEditionCode(String editionCode) {
		Query query = getSession().createQuery("SELECT p FROM Plan p WHERE p.editionCode=:editionCode").setParameter("editionCode", editionCode);
        return  (Plan) query.uniqueResult();
	}

	@Override
	public MarketPlace getMarketPlaceByUrlAndPartner(String baseUrl, String partner) {
		Query query = getSession().createQuery("SELECT m FROM MarketPlace m WHERE m.baseUrl=:baseUrl and m.partner=:partner")
				.setParameter("baseUrl", baseUrl).setParameter("partner", partner);
        return  (MarketPlace) query.uniqueResult();
	}

}
