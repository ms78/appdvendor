package com.appdvendor.eventhandler.dao;

import com.appdvendor.eventhandler.model.Company;
import com.appdvendor.eventhandler.model.MarketPlace;
import com.appdvendor.eventhandler.model.Plan;
import com.appdvendor.eventhandler.model.Subscription;
import com.appdvendor.eventhandler.model.User;

/**
 * Main interface to operate on user account.
 *  
 * @author Muthanna
 */
public interface AccountDao extends BaseDao {

	User getUserByAppDirectUuid(String uuid);
	
	Company getCompanyByAppDirectUuid(String uuid);
	
	User getUserByOpenId(String openId);

	Subscription getSubscriptionById(String id);
	
	Plan getPlanByEditionCode(String editionCode);
	
	MarketPlace getMarketPlaceByUrlAndPartner(String baseUrl, String partner);
}
