package com.appdvendor.eventhandler.dao;

import java.util.List;

/**
 * Parent DAO interface that relies on generics to execute
 * common data access operations.
 * 
 * @author Muthanna
 */
public interface BaseDao {

	<T> void save(final T t);

	<T> void delete(final T t);

	<T> T merge(final T t);

	<T> void saveOrUpdate(final T t);

	<T> T get(final Class<T> clazz, final Long id);
	
	<T> List<T> getAll(final Class<T> clazz);
	
	<T> List<T> getAll(final Class<T> clazz, Integer start, Integer max);

}
