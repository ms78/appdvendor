package com.appdvendor.eventhandler.dao.hibernate;

import java.util.Collections;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;

import com.appdvendor.eventhandler.dao.BaseDao;

/**
 * A Hibernate based implementation of {@link BaseDao}.
 * 
 * @author Muthanna
 */
class BaseDaoHibernateImpl implements BaseDao {

	private final Logger logger = LogManager.getLogger(BaseDaoHibernateImpl.class);
	
	private SessionFactory sessionFactory;
	
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	protected Session getSession() {
		return sessionFactory.getCurrentSession();
	}

    public <T> void save(final T t){
    	getSession().save(t);
    }

    public <T> void delete(final T t) {
      getSession().delete(t);
    }

    /***/
    public <T> T get(final Class<T> clazz, final Long id){
      return (T) getSession().get(clazz, id);
    }

    @SuppressWarnings("unchecked")
    public <T> T merge(final T t)   {
      return (T) getSession().merge(t);
    }

    /***/
    public <T> void saveOrUpdate(final T t){
      getSession().saveOrUpdate(t);
    }

    public <T> List<T> getAll(final Class<T> clazz) {
      Criteria criteria = getSession().createCriteria(clazz);
      criteria.addOrder(Order.desc("id"));
      criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);  
      return getFromCriteria(criteria);
    }
    
	@Override
    public <T> List<T> getAll(final Class<T> clazz, Integer start, Integer max) {
    	Criteria criteria = getSession().createCriteria(clazz);
    	criteria.addOrder(Order.desc("id"));
    	
        if (start != null) {
        	criteria.setFirstResult(start);
        }
        
        if (max != null) {
        	criteria.setMaxResults(max);
        }
        
        criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
        
        return getFromCriteria(criteria);
    }
    
	@SuppressWarnings("unchecked")
    protected <T> List<T> getFromCriteria(Criteria criteria) {
    	try {
    		criteria.setCacheable(true);
			return criteria.list();
		} catch (HibernateException e) {
			logger.error("Database access error while executing criteria:" + e);
		}
    	
    	return Collections.emptyList();
    }
    
}
