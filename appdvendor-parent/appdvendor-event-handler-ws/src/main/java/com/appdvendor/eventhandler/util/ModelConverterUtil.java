package com.appdvendor.eventhandler.util;

import java.util.ArrayList;
import java.util.List;

import com.appdvendor.eventhandler.model.Company;
import com.appdvendor.eventhandler.model.Item;
import com.appdvendor.eventhandler.model.MarketPlace;
import com.appdvendor.eventhandler.model.Plan;
import com.appdvendor.eventhandler.model.Product;
import com.appdvendor.eventhandler.model.User;
import com.appdvendor.eventhandler.model.UserType;
import com.appdvendor.rest.appd.api.ADCompany;
import com.appdvendor.rest.appd.api.ADEvent;
import com.appdvendor.rest.appd.api.ADItem;
import com.appdvendor.rest.appd.api.ADOrder;
import com.appdvendor.rest.appd.api.ADUser;

/**
 * Offers static methods to convert between the DB entities and the models from
 * AppDirect API.
 * 
 * @author Muthanna
 */
public final class ModelConverterUtil {

	private ModelConverterUtil() {
	}

	public static MarketPlace buildMarketPlace(ADEvent event) {
		MarketPlace marketPlace = new MarketPlace();
		marketPlace.setBaseUrl(event.getMarketplace().getBaseUrl());
		marketPlace.setPartner(event.getMarketplace().getPartner());

		return marketPlace;
	}

	public static Plan buildPlan(ADEvent event, Product product) {
		ADOrder order = event.getPayload().getOrder();
		Plan plan = new Plan();
		plan.setName(order.getEditionCode());
		plan.setEditionCode(order.getEditionCode());
		plan.setPricingDuration(order.getPricingDuration());
		plan.setProduct(product);

		return plan;
	}

	public static Company buildCompany(ADEvent event) {
		ADCompany companyAD = event.getPayload().getCompany();
		if (event.getPayload() == null || event.getPayload().getCompany() == null)
			return null;

		Company company = new Company();
		company.setUuid(companyAD.getUuid());
		company.setEmail(companyAD.getEmail());
		company.setCountry(companyAD.getCountry());
		company.setName(companyAD.getName());
		company.setPhone(companyAD.getPhoneNumber());
		company.setWebsite(companyAD.getWebsite());
		return company;
	}

	public static User buildUser(ADUser user, Company company) {
		if (user == null)
			return null;

		User creator = new User();
		creator.setEmail(user.getEmail());
		creator.setFirstName(user.getFirstName());
		creator.setLanguage(user.getLanguage());
		creator.setLastName(user.getLastName());
		creator.setOpenId(user.getOpenId());
		creator.setUuid(user.getUuid());
		creator.setType(UserType.U);
		creator.setCompany(company);

		return creator;
	}

	public static List<Item> buildItems(ADEvent event) {
		if (event.getPayload() == null || event.getPayload().getOrder() == null)
			return new ArrayList<>();

		List<Item> items = new ArrayList<>();
		for (ADItem itemAD : event.getPayload().getOrder().getItems()) {
			Item item = new Item();
			item.setQuantity(itemAD.getQuantity());
			item.setUnit(itemAD.getUnit());
			items.add(item);
		}

		return items;
	}

}
