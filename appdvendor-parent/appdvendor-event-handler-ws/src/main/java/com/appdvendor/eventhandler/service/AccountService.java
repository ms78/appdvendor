package com.appdvendor.eventhandler.service;

import java.util.List;

import com.appdvendor.eventhandler.model.Company;
import com.appdvendor.eventhandler.model.MarketPlace;
import com.appdvendor.eventhandler.model.Plan;
import com.appdvendor.eventhandler.model.Product;
import com.appdvendor.eventhandler.model.Subscriber;
import com.appdvendor.eventhandler.model.Subscription;
import com.appdvendor.eventhandler.model.User;

/**
 * Main service to manage account info in the database.
 * 
 * @author Muthanna
 */
public interface AccountService {

	User getUserByOpenId(String openId);
	User getUserByAppDirectUuid(String uuid);
	void saveUser(User user);
	void deleteUser(User user);
	
	void saveCompany(Company company);
	Company getCompanyByAppDirectUuid(String uuid);
	List<Company> getCompanies();

	Subscription getSubscriptionById(String id);
	void saveSubscription(Subscription subscription);
	void deleteSubscriber(Subscriber subscriber);
	Plan getPlanByEditionCode(String editionCode);
	void savePlan(Plan plan);
	List<Product> getProducts();
	List<Subscription> getSubscriptions();


	MarketPlace getMarketPlaceByUrlAndPartner(String baseUrl, String partner);
	void saveMarketPlace(MarketPlace marketPlace);
	
}
