package com.appdvendor.eventhandler.exception;

import com.appdvendor.rest.appd.api.ADEventHandlingError;

/**
 * Exception thrown when something wrong happens while
 * handling AppDirect events.
 * 
 * @author Muthanna
 */
public class EventHandlingException extends Exception {

	private static final long serialVersionUID = -2308042225025921999L;

	private ADEventHandlingError error;
	
	public EventHandlingException(String message, ADEventHandlingError error) {
        this(message, error, null);
    }
	
	public EventHandlingException(String message, ADEventHandlingError error, Throwable cause) {
        super(message, cause);
        this.error = error;
    }
	
	public ADEventHandlingError getError() {
		return error;
	}
	
}
