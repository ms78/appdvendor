package com.appdvendor.eventhandler.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.appdvendor.eventhandler.service.AccountService;

/**
 * Display home page with a summary of the vendor products, users, companies
 * and subscriptions.
 * @author Muthanna
 */
@Controller
public class RootController {

	@Autowired
	private AccountService accountService;

	@RequestMapping("/")
	public String home(Model model) {

		model.addAttribute("products", accountService.getProducts());
		model.addAttribute("companies", accountService.getCompanies());
		model.addAttribute("subscriptions", accountService.getSubscriptions());

		return "home";
	}

}
