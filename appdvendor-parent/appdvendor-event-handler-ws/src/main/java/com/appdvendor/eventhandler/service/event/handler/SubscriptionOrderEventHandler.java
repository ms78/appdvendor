package com.appdvendor.eventhandler.service.event.handler;

import java.util.List;

import com.appdvendor.eventhandler.model.Company;
import com.appdvendor.eventhandler.model.Item;
import com.appdvendor.eventhandler.model.MarketPlace;
import com.appdvendor.eventhandler.model.Plan;
import com.appdvendor.eventhandler.model.Subscription;
import com.appdvendor.eventhandler.model.SubscriptionStatus;
import com.appdvendor.eventhandler.model.User;
import com.appdvendor.eventhandler.util.ModelConverterUtil;
import com.appdvendor.rest.appd.api.ADEvent;
import com.appdvendor.rest.appd.api.ADEventHandlingResult;
import com.appdvendor.rest.appd.api.ADEventHandlingSuccessResult;

/**
 * Handles subscription create.
 * 
 * @author Muthanna
 */
public class SubscriptionOrderEventHandler extends AbstractEventHandler {

	@Override
	public ADEventHandlingResult process(ADEvent event) {

		Subscription subscription = new Subscription();
		
		MarketPlace marketPlace = getAccountService().getMarketPlaceByUrlAndPartner(event.getMarketplace().getBaseUrl(),
				event.getMarketplace().getPartner());
		
		if(marketPlace == null) {
			marketPlace = ModelConverterUtil.buildMarketPlace(event);
			getAccountService().saveMarketPlace(marketPlace);
		}
		subscription.setMarketPlace(marketPlace);
		
		Company company = getAccountService().getCompanyByAppDirectUuid(event.getPayload().getCompany().getUuid());
		if(company == null) {
			company = ModelConverterUtil.buildCompany(event);
			getAccountService().saveCompany(company);
		}
		subscription.setCompany(company);
		
		User user = getAccountService().getUserByAppDirectUuid(event.getCreator().getUuid());
		if(user == null) {
			user = ModelConverterUtil.buildUser(event.getCreator(), company);
			getAccountService().saveUser(user);
		}
		subscription.setCreator(user);
		
		Plan plan = getAccountService().getPlanByEditionCode(event.getPayload().getOrder().getEditionCode());
		if(plan == null) {
			plan = ModelConverterUtil.buildPlan(event, getAccountService().getProducts().get(0));
			getAccountService().savePlan(plan);;
		}
        subscription.setPlan(plan);
        
        List<Item> items = ModelConverterUtil.buildItems(event);
        subscription.setItems(items);
        
		subscription.setStatus(SubscriptionStatus.ACTIVE);

		getAccountService().saveSubscription(subscription);
		return new ADEventHandlingSuccessResult(subscription.getIdAsString());

	}

}
