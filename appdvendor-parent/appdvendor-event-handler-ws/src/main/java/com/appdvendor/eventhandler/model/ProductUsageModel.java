package com.appdvendor.eventhandler.model;

/**
 * 
 * @author Muthanna
 */
public enum ProductUsageModel {
	SIGNLE_USER, 
	MULTIPLE_USERS;
}
