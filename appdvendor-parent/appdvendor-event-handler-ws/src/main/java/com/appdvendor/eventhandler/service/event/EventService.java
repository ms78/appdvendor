package com.appdvendor.eventhandler.service.event;

import com.appdvendor.eventhandler.exception.EventHandlingException;
import com.appdvendor.rest.appd.api.ADEventHandlingContext;
import com.appdvendor.rest.appd.api.ADEventHandlingResult;

/**
 * Service interface to fetch and handle events coming from AppDirect.
 * 
 * @author Muthanna
 */
public interface EventService {

	ADEventHandlingResult execute(ADEventHandlingContext context) throws EventHandlingException;
	
}
