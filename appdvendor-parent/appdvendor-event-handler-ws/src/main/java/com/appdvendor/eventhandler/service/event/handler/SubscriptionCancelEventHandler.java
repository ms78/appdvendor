package com.appdvendor.eventhandler.service.event.handler;

import com.appdvendor.eventhandler.exception.EventHandlingException;
import com.appdvendor.eventhandler.model.Subscription;
import com.appdvendor.eventhandler.model.SubscriptionStatus;
import com.appdvendor.rest.appd.api.ADEvent;
import com.appdvendor.rest.appd.api.ADEventHandlingError;
import com.appdvendor.rest.appd.api.ADEventHandlingResult;
import com.appdvendor.rest.appd.api.ADEventHandlingSuccessResult;

/**
 * Handles subscription cancel.
 *  
 * @author Muthanna
 */
public class SubscriptionCancelEventHandler extends AbstractEventHandler {

	@Override
	public ADEventHandlingResult process(ADEvent aDEvent) throws EventHandlingException {

		Subscription subscription = getAccountService().getSubscriptionById(aDEvent.getPayload().getAccount().getAccountIdentifier());
		if (subscription == null) {
			throw new EventHandlingException("Account not found!", ADEventHandlingError.ACCOUNT_NOT_FOUND);
		}
		
		subscription.setStatus(SubscriptionStatus.CANCELLED);
		getAccountService().saveSubscription(subscription);
		return new ADEventHandlingSuccessResult(subscription.getIdAsString());
	}

}
