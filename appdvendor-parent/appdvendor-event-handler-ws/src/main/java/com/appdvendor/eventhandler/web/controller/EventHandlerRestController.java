package com.appdvendor.eventhandler.web.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.appdvendor.eventhandler.exception.EventHandlingException;
import com.appdvendor.eventhandler.service.event.EventService;
import com.appdvendor.rest.appd.api.ADEventHandlingContext;
import com.appdvendor.rest.appd.api.ADEventHandlingResult;

/**
 * Main rest controller that will receive events from AppDirect 
 * and delegate handling to {@link EventService}.
 * 
 * @author Muthanna
 */
@RestController
@RequestMapping("/events/")
public class EventHandlerRestController {
	
	@Autowired
	private EventService eventService;
	
	@RequestMapping(value = "/handle", method = RequestMethod.GET)
	public ADEventHandlingResult processEvent(HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value = "eventUrl", required = false) String eventUrl,
			@RequestParam(value = "token", required = false) String token) throws EventHandlingException {
		
		ADEventHandlingContext context = new ADEventHandlingContext();
		context.setEventUrl(eventUrl);
		context.setToken(token);
		
		return eventService.execute(context);
	}
	
}
