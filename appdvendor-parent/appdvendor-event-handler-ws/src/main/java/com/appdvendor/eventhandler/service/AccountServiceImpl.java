package com.appdvendor.eventhandler.service;

import java.util.List;

import com.appdvendor.eventhandler.dao.AccountDao;
import com.appdvendor.eventhandler.model.Company;
import com.appdvendor.eventhandler.model.MarketPlace;
import com.appdvendor.eventhandler.model.Plan;
import com.appdvendor.eventhandler.model.Product;
import com.appdvendor.eventhandler.model.Subscriber;
import com.appdvendor.eventhandler.model.Subscription;
import com.appdvendor.eventhandler.model.User;

/**
 * Implementation of {@link AccountService}.
 * 
 * @author Muthanna
 */
public class AccountServiceImpl implements AccountService {

	private AccountDao accountDao;
	
	public void setAccountDao(AccountDao accountDao) {
		this.accountDao = accountDao;
	}
	
	@Override
	public User getUserByAppDirectUuid(String uuid) {
		return accountDao.getUserByAppDirectUuid(uuid);
	}
	
	@Override
	public User getUserByOpenId(String openId) {
		return accountDao.getUserByOpenId(openId);
	}

	@Override
	public void saveUser(User user) {
		accountDao.save(user);
	}
	
	@Override
	public void deleteUser(User user) {
		accountDao.delete(user);
	}
	
	@Override
	public void saveCompany(Company company) {
		accountDao.save(company);
	}
	
	@Override
	public Company getCompanyByAppDirectUuid(String uuid) {
		return accountDao.getCompanyByAppDirectUuid(uuid);
	}
	
	@Override
	public List<Company> getCompanies() {
		return accountDao.getAll(Company.class);
	}
	
	@Override
	public Subscription getSubscriptionById(String id) {
		return accountDao.getSubscriptionById(id);
	}

	@Override
	public void saveSubscription(Subscription subscription) {
		accountDao.save(subscription);
	}

	@Override
	public Plan getPlanByEditionCode(String editionCode) {
		return accountDao.getPlanByEditionCode(editionCode);
	}
	
	@Override
	public void savePlan(Plan plan) {
		accountDao.save(plan);
	}

	@Override
	public void deleteSubscriber(Subscriber subscriber) {
		accountDao.delete(subscriber);
	}
	
	@Override
	public List<Product> getProducts() {
		return accountDao.getAll(Product.class);
	}

	@Override
	public MarketPlace getMarketPlaceByUrlAndPartner(String baseUrl, String partner) {
		return accountDao.getMarketPlaceByUrlAndPartner(baseUrl, partner);
	}

	@Override
	public void saveMarketPlace(MarketPlace marketPlace) {
		accountDao.save(marketPlace);
	}

	@Override
	public List<Subscription> getSubscriptions() {
		return accountDao.getAll(Subscription.class);
	}
	
}
