package com.appdvendor.eventhandler.service.event.handler;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.appdvendor.eventhandler.exception.EventHandlingException;
import com.appdvendor.eventhandler.service.AccountService;
import com.appdvendor.rest.appd.api.ADEvent;
import com.appdvendor.rest.appd.api.ADEventHandlingError;
import com.appdvendor.rest.appd.api.ADEventHandlingResult;

/**
 * An abstract event handler for AppDirect events. It is designed
 * as a template to do some sanity checks and exception handling while
 * delegate the custom processing to the custom logic it each sub-class.
 * 
 * @author Muthanna
 */
public abstract class AbstractEventHandler implements EventHandler {

	protected static final Logger logger = LogManager.getLogger(AbstractEventHandler.class);
	
	protected AccountService accountService;

	public void setAccountService(AccountService accountService) {
		this.accountService = accountService;
	}
	
	public AccountService getAccountService() {
		return accountService;
	}
	
	@Override
	public ADEventHandlingResult handle(ADEvent event) throws EventHandlingException {

		if(event == null) {
			String err = "Null events are not allowed";
			logger.error(err);
			throw new EventHandlingException(err, ADEventHandlingError.TRANSPORT_ERROR);
		}
		
		logger.info("Received a valid event of type: {}", event.getType());

		// Save things in DB and return
		return process(event);
	}
	
	protected abstract ADEventHandlingResult process(ADEvent event) throws EventHandlingException;
}
