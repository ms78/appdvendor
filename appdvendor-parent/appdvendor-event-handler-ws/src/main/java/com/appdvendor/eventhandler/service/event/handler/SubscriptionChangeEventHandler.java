package com.appdvendor.eventhandler.service.event.handler;

import java.util.List;

import com.appdvendor.eventhandler.exception.EventHandlingException;
import com.appdvendor.eventhandler.model.Item;
import com.appdvendor.eventhandler.model.Plan;
import com.appdvendor.eventhandler.model.Subscription;
import com.appdvendor.eventhandler.util.ModelConverterUtil;
import com.appdvendor.rest.appd.api.ADEvent;
import com.appdvendor.rest.appd.api.ADEventHandlingError;
import com.appdvendor.rest.appd.api.ADEventHandlingResult;
import com.appdvendor.rest.appd.api.ADEventHandlingSuccessResult;

/**
 * Handles subscription change.
 * 
 * @author Muthanna
 */
public class SubscriptionChangeEventHandler extends AbstractEventHandler {

	@Override
	public ADEventHandlingResult process(ADEvent aDEvent) throws EventHandlingException {

		Subscription subscription = getAccountService().getSubscriptionById(aDEvent.getPayload().getAccount().getAccountIdentifier());
		if (subscription == null) {
			throw new EventHandlingException("Account not found!", ADEventHandlingError.ACCOUNT_NOT_FOUND);
		}
		
		Plan plan = getAccountService().getPlanByEditionCode(aDEvent.getPayload().getOrder().getEditionCode());
		if(plan == null) {
			plan = ModelConverterUtil.buildPlan(aDEvent, getAccountService().getProducts().get(0));
			getAccountService().savePlan(plan);;
		}
        subscription.setPlan(plan);

        List<Item> items = ModelConverterUtil.buildItems(aDEvent);
        subscription.setItems(items);
		
		getAccountService().saveSubscription(subscription);
		return new ADEventHandlingSuccessResult(subscription.getIdAsString());
	}

}
