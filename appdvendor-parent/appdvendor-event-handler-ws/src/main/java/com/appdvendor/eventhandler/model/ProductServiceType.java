package com.appdvendor.eventhandler.model;

/**
 * 
 * @author Muthanna
 */
public enum ProductServiceType {
	STANDALONE_PRODUCT, 
	ADDON_FOR_PRODUCT;
}
