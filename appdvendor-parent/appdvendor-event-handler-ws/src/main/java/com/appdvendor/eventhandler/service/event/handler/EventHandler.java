package com.appdvendor.eventhandler.service.event.handler;

import com.appdvendor.eventhandler.exception.EventHandlingException;
import com.appdvendor.rest.appd.api.ADEvent;
import com.appdvendor.rest.appd.api.ADEventHandlingResult;

/**
 * Event handler interface.
 * 
 * @author Muthanna
 */
public interface EventHandler {

	/**
	 * Handles the event from AppDirect and return result to the caller
	 * @param event AppDirect event
	 * @return a success or error instance of {@link ADEventHandlingResult}
	 * @throws EventHandlingException an exception to indicate errors in handling the event
	 */
	ADEventHandlingResult handle(ADEvent event) throws EventHandlingException;
	
}
