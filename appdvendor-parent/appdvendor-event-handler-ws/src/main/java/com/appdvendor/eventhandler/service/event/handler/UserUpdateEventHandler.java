package com.appdvendor.eventhandler.service.event.handler;

import com.appdvendor.eventhandler.exception.EventHandlingException;
import com.appdvendor.eventhandler.model.Subscription;
import com.appdvendor.eventhandler.model.User;
import com.appdvendor.rest.appd.api.ADEvent;
import com.appdvendor.rest.appd.api.ADEventHandlingError;
import com.appdvendor.rest.appd.api.ADEventHandlingResult;
import com.appdvendor.rest.appd.api.ADEventHandlingSuccessResult;
import com.appdvendor.rest.appd.api.ADUser;

/**
 * Handles user update.
 * 
 * @author Muthanna
 */
public class UserUpdateEventHandler extends AbstractEventHandler {

	@Override
	public ADEventHandlingResult process(ADEvent event) throws EventHandlingException {

		// Make sure we've got a valid account
		Subscription subscription = getAccountService().getSubscriptionById(event.getPayload().getAccount().getAccountIdentifier());
		if (subscription == null) 
			throw new EventHandlingException("Account not found!", ADEventHandlingError.ACCOUNT_NOT_FOUND);
		
		// Make sure user already exists
		ADUser adUser = event.getPayload().getUser();
		User user = getAccountService().getUserByAppDirectUuid(adUser.getUuid());
		if (user == null)
			throw new EventHandlingException("User not found!", ADEventHandlingError.USER_NOT_FOUND);

		// Update the user
		user.setFirstName(adUser.getFirstName());
		user.setLastName(adUser.getLastName());
		user.setLanguage(adUser.getLanguage());
		user.setOpenId(adUser.getOpenId());
		user.setUuid(adUser.getUuid());
		user.setEmail(adUser.getEmail());
		
		getAccountService().saveUser(user);
		
		return new ADEventHandlingSuccessResult();
	}

}
