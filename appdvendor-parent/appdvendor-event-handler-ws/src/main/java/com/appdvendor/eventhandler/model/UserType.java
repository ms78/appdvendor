package com.appdvendor.eventhandler.model;

/**
 * 
 * @author Muthanna
 */
public enum UserType {
	A, // Admin
	U; // User
}
