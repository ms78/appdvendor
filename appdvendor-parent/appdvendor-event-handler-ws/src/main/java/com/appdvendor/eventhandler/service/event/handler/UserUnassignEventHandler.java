package com.appdvendor.eventhandler.service.event.handler;

import com.appdvendor.eventhandler.exception.EventHandlingException;
import com.appdvendor.eventhandler.model.Subscriber;
import com.appdvendor.eventhandler.model.Subscription;
import com.appdvendor.eventhandler.model.User;
import com.appdvendor.rest.appd.api.ADEvent;
import com.appdvendor.rest.appd.api.ADEventHandlingError;
import com.appdvendor.rest.appd.api.ADEventHandlingErrorResult;
import com.appdvendor.rest.appd.api.ADEventHandlingResult;
import com.appdvendor.rest.appd.api.ADEventHandlingSuccessResult;
import com.appdvendor.rest.appd.api.ADPayload;

/**
 * Handles user unassign.
 * 
 * @author Muthanna
 */
public class UserUnassignEventHandler extends AbstractEventHandler {

	@Override
	public ADEventHandlingResult process(ADEvent aDEvent) throws EventHandlingException {
		
		// Check we have got a valid user
		ADPayload aDPayload = aDEvent.getPayload();
		User user = getAccountService().getUserByAppDirectUuid(aDPayload.getUser().getUuid());
		if (user == null)
			throw new EventHandlingException("User not found!", ADEventHandlingError.USER_NOT_FOUND);
		
		// Check we have got a valid user subscription
		Subscription subscription = getAccountService().getSubscriptionById(aDEvent.getPayload().getAccount().getAccountIdentifier());
		if (subscription == null) 
			throw new EventHandlingException("Account not found!", ADEventHandlingError.ACCOUNT_NOT_FOUND);
		
		// Check if the user is already assigned or else return error
		Subscriber toRemove = null;
		for(Subscriber s : subscription.getSubscribers()) {
			if(user.equals(s.getSubscriber())) {
				toRemove = s;
				break;
			}
		}
		if (toRemove == null)
			return new ADEventHandlingErrorResult(ADEventHandlingError.BINDING_NOT_FOUND);

		// Unassign and save
		subscription.getSubscribers().remove(toRemove);
		getAccountService().deleteSubscriber(toRemove);
		
		return new ADEventHandlingSuccessResult();
	}

}
