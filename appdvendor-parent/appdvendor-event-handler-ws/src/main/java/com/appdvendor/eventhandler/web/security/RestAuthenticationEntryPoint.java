package com.appdvendor.eventhandler.web.security;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.oauth.provider.OAuthProcessingFilterEntryPoint;

/**
 * OAuth processing entry point to deal with authentication exceptions and
 * return unauthorized response code to the caller.
 * 
 * @author Muthanna
 */
public class RestAuthenticationEntryPoint extends OAuthProcessingFilterEntryPoint {

	private final static Logger logger = LogManager.getLogger(RestAuthenticationEntryPoint.class);

    @Override
    public void commence(HttpServletRequest request, HttpServletResponse response, AuthenticationException authException) throws IOException {
    	if (logger.isDebugEnabled()) {
			logger.debug("Rest authentication entry point called. Rejecting access");
		}
        response.sendError(HttpServletResponse.SC_UNAUTHORIZED, "Unauthorized: " + authException.getMessage());
    }
}