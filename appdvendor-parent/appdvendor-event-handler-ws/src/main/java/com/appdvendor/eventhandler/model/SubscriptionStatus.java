package com.appdvendor.eventhandler.model;

/**
 * 
 * @author Muthanna
 */
public enum SubscriptionStatus {
	ACTIVE, 
	CANCELLED;
}
