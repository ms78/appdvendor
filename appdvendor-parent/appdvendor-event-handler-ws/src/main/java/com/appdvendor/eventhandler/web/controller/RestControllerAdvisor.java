package com.appdvendor.eventhandler.web.controller;

import javax.servlet.http.HttpServletRequest;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;

import com.appdvendor.eventhandler.exception.EventHandlingException;
import com.appdvendor.rest.appd.api.ADEventHandlingError;
import com.appdvendor.rest.appd.api.ADEventHandlingErrorResult;
import com.appdvendor.rest.appd.api.ADEventHandlingResult;

/**
 * A general error handling advisor to act on all rest controllers in the application.
 * 
 * @author Muthanna
 */
@ControllerAdvice(annotations = {RestController.class})
@Order(Ordered.HIGHEST_PRECEDENCE)
public class RestControllerAdvisor {

	private static final Logger logger = LogManager.getLogger(RestControllerAdvisor.class);
	
	@ExceptionHandler(Throwable.class)
    public ResponseEntity<ADEventHandlingResult> handleError(Throwable exception, WebRequest request, HttpServletRequest req) {
		
		logger.error("Rest API encountered an error: ", exception);
		
		ADEventHandlingResult result = null;
		if(exception instanceof EventHandlingException) {
			EventHandlingException e = (EventHandlingException) exception;
			result = new ADEventHandlingErrorResult(e.getError(), e.getMessage());;
		} else {
			result = new ADEventHandlingErrorResult(ADEventHandlingError.UNKNOWN_ERROR, 
					"Something went wrong while serving the request.");
		}
		
		// Build the error response
        return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }
	
}
