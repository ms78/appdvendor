package com.appdvendor.eventhandler.model;

import java.util.List;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * 
 * @author Muthanna
 */
@Entity
@Table(name = "product")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Product {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Basic(optional = false)
	@Column(name = "id")
	private Long id;

	@Column(name = "uuid")
	private String uuid;

	@Column(name = "name")
	private String name;

	@Column(name = "service_type")
	@Enumerated(EnumType.STRING)
	private ProductServiceType serviceType;

	@Column(name = "usage_model")
	@Enumerated(EnumType.STRING)
	private ProductUsageModel usageModel;

	@Column(name = "revenue_model")
	@Enumerated(EnumType.STRING)
	private ProductRevenueModel revenueModel;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "market_place_id")
	private MarketPlace marketPlace;
	
	@OneToMany(fetch = FetchType.LAZY, cascade = { CascadeType.ALL })
	@JoinColumn(name = "product_id")
	private List<Plan> plans;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public ProductServiceType getServiceType() {
		return serviceType;
	}

	public void setServiceType(ProductServiceType serviceType) {
		this.serviceType = serviceType;
	}

	public ProductUsageModel getUsageModel() {
		return usageModel;
	}

	public void setUsageModel(ProductUsageModel usageModel) {
		this.usageModel = usageModel;
	}

	public ProductRevenueModel getRevenueModel() {
		return revenueModel;
	}

	public void setRevenueModel(ProductRevenueModel revenueModel) {
		this.revenueModel = revenueModel;
	}
	
	public MarketPlace getMarketPlace() {
		return marketPlace;
	}

	public void setMarketPlace(MarketPlace marketPlace) {
		this.marketPlace = marketPlace;
	}

	public List<Plan> getPlans() {
		return plans;
	}

	public void setPlans(List<Plan> plans) {
		this.plans = plans;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((revenueModel == null) ? 0 : revenueModel.hashCode());
		result = prime * result + ((serviceType == null) ? 0 : serviceType.hashCode());
		result = prime * result + ((usageModel == null) ? 0 : usageModel.hashCode());
		result = prime * result + ((uuid == null) ? 0 : uuid.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Product other = (Product) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (revenueModel != other.revenueModel)
			return false;
		if (serviceType != other.serviceType)
			return false;
		if (usageModel != other.usageModel)
			return false;
		if (uuid == null) {
			if (other.uuid != null)
				return false;
		} else if (!uuid.equals(other.uuid))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Product [id=" + id + ", uuid=" + uuid + ", name=" + name + ", serviceType=" + serviceType
				+ ", usageModel=" + usageModel + ", revenueModel=" + revenueModel + "]";
	}

}
